# Ice polycristal models documentation

[![Jupyter Book Badge](https://jupyterbook.org/badge.svg)](https://mecaiceige.gricad-pages.univ-grenoble-alpes.fr/tools/ice-polycrystal-models/ipms_documentation/Intro.html)
[![pipeline status](https://gricad-gitlab.univ-grenoble-alpes.fr/mecaiceige/tools/ice-polycrystal-models/ipms_documentation/badges/main/pipeline.svg)](https://gricad-gitlab.univ-grenoble-alpes.fr/mecaiceige/tools/ice-polycrystal-models/ipms_documentation/-/commits/main)
[![PyPI version](https://img.shields.io/badge/license-GPLv3-blue.svg)](https://gricad-gitlab.univ-grenoble-alpes.fr/mecaiceige/tools/ice-polycrystal-models/ipms_documentation/-/blob/main/LICENSE)