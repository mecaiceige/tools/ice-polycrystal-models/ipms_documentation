# Welcome to the ice polycrystal modelling tools

[![Jupyter Book Badge](https://jupyterbook.org/badge.svg)](https://mecaiceige.gricad-pages.univ-grenoble-alpes.fr/tools/ice-polycrystal-models/ipms_documentation/Intro.html)
[![pipeline status](https://gricad-gitlab.univ-grenoble-alpes.fr/mecaiceige/tools/ice-polycrystal-models/ipms_documentation/badges/main/pipeline.svg)](https://gricad-gitlab.univ-grenoble-alpes.fr/mecaiceige/tools/ice-polycrystal-models/ipms_documentation/-/commits/main)
[![PyPI version](https://img.shields.io/badge/license-GPLv3-blue.svg)](https://gricad-gitlab.univ-grenoble-alpes.fr/mecaiceige/tools/ice-polycrystal-models/ipms_documentation/-/blob/main/LICENSE)

This project is aiming to provide numerical tools to performed simulation within two framework :

- CraFT : with an **elasto-viscoplastic crystal plasticity** behavior based on [CraFT](https://lma-software-craft.cnrs.fr/)
- R³iCe with a **Continious Transverse Isotropic** behavior based on [rheolef](https://membres-ljk.imag.fr/Pierre.Saramito/rheolef/html/index.html)

Other tools are provided in order to create input file for both models such as mesh and orientation files.

- mesh tools : based on [neper](https://neper.info/) and [gmsh](http://gmsh.info/)
- orientation tools : based on [MTEX](https://mtex-toolbox.github.io/)

## Numerical Models

::::{grid} 1 1 1 2
:class-container: text-center 
:gutter: 3

:::{grid-item-card} 
:link: docs/CraFT_doc/craft_main
:link-type: doc
:class-header: bg-light

CraFT
^^^

```{image} docs/CraFT_doc/logo_CraFT.png
:alt: aita_pf
:class: bg-primary mb-1
:width: 200px
:align: center
```

Elasto-viscoplastic crystal plasticity behavior

:::

:::{grid-item-card} 
:link: docs/Rheolef_doc/rheolef_main
:link-type: doc
:class-header: bg-light

R³iCe
^^^

```{image} docs/Rheolef_doc/logo_R3iCe.png
:alt: aita_pf
:class: bg-primary mb-1
:width: 200px
:align: center
```

Continious Transverse Isotropic behavior
:::

::::

## Utils

::::{grid} 1 1 1 2
:class-container: text-center 
:gutter: 3

:::{grid-item-card} 
:link: docs/Mesh_Generator_doc/mesh_main
:link-type: doc
:class-header: bg-light

Mesh generator
^^^

Create structured and unstructured mesh for both CraFT and R³iCe

:::

:::{grid-item-card} 
:link: docs/Orientation_Generator_doc/ori_main
:link-type: doc
:class-header: bg-light

Orientation generator
^^^

Create orientations from ODF
:::

::::
