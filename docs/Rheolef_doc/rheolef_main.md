# R3iCe


![GitLab tag (self-managed)](https://img.shields.io/gitlab/v/tag/mecaiceige/tools/ice-polycrystal-models/rheolef_cti?gitlab_url=https%3A%2F%2Fgricad-gitlab.univ-grenoble-alpes.fr&include_prereleases&sort=date)
[![PyPI version](https://img.shields.io/badge/license-GPLv3-blue.svg)](https://gricad-gitlab.univ-grenoble-alpes.fr/mecaiceige/tools/ice-polycrystal-models/rheolef_cti/-/blob/main/LICENSE)

::::{grid} 1 1 2 2
:class-container: text-center 
:gutter: 3


:::{grid-item-card} 
:link: rheolef_main
:link-type: doc
:class-header: bg-info

R3iCe formulation
^^^

:::



:::{grid-item-card} 
:link: rheolef_install
:link-type: doc
:class-header: bg-light

Install
^^^
:::

:::{grid-item-card} 
:link: rheolef_tutorial
:link-type: doc
:class-header: bg-light

Tutorial : run simulation
^^^

:::

:::{grid-item-card} 
:link: rheolef_output
:link-type: doc
:class-header: bg-light

Simulation output
^^^
:::

::::


# CTI formulation


How to build the CTI law is describe in the page : [{bdg-primary}`CTI demonstration`](rheolef_cti_demo_linear)


::::{grid} 1 1 1 2
:class-container: text-center 
:gutter: 3


:::{grid-item-card} 
:link: rheolef_main
:link-type: doc
:class-header: .bg-warning

CTI formulation
^^^

:::



:::{grid-item-card} 
:link: rheolef_numeric
:link-type: doc
:class-header: .bg-body

Numerical implementation
^^^
:::

::::

## Description of the Continuous Transverse Isotropic (CTI) formulation

A transverse isotropic material is a material with a different behavior in one main direction $e_3$ and perpendicular to this direction. But the properties perpendicular to $e_3$ are identical. Therefore the properties transverse to $e_3$ are isotropic.

Such formulation can be suitable model for modelling single crystal behavior for hexagonal material such as ice, quartz, magnesium alloy. 

```{figure} cti_geom.svg
---
height: 150px
name: directive-fig
---
Transverse isotropic representation. The vector $e_3$ gives the principal direction.
```

### Single crystal linear behavior

- $\eta_1$ is the viscosity for shear parallel to the basal plane

$$S_{13}=2 \eta_1 D_{13}, \quad S_{23}=2 \eta_1 D_{23}$$

- $\beta$ is the viscosity ratio between shear parallel a basal plane and within the basal plane

$$S_{12}=2\frac{\eta_1}{\beta} D_{12}$$

- $\gamma$ is the viscosity ratio between compression (or traction) along the $e_3$ axis ($\eta'$) and in one direction perpendicular $e_r$

$$S_{33}=2 \eta' D_{33}, \quad \gamma S_{rr}=2 \eta' D_{rr}$$

Therefore for a same solicitation in different direction $S=S_{33}=S_{11}$; it gives :

$$ D_{rr}=\gamma D_{33}$$

### CTI equation

````{tab-set}
```{tab-item} Dimensionless equations

:::{note}
To make the equation dimensionless we use :

- typical viscosity $\eta_n \sim Pa.s^{\frac{1}{n}}$

1. If stress boundary condition is applied :
    - typical stress $\Sigma\sim Pa$.

    Therefore a characteristic time $T$ is defined as :

    - $T=\left(\frac{\eta_n}{\Sigma}\right)^{n}$
2. If strain rate condition is applied :
    - typical strain rate : $\dot{\varepsilon}_m\sim s^{-1}$
    - $T=\frac{1}{\dot{\varepsilon}_m}\sim s$
    - $\Sigma=\eta_n \dot{\varepsilon}_m^{\frac{1}{n}}\sim Pa$
:::
This is the general form for the CTI equation. The linear formulation is obtained with $n=1$.

$$\tilde{S}=\tilde{\eta}^\star\left(2\alpha_1 \tilde{D} + 2\alpha_2 M^D Tr(MD) + \alpha_3 (M\tilde{D}+\tilde{D}M)^D\right)$$

$$\tilde{\eta}^\star=2\left(\alpha_1 tr(\tilde{D}^2) + \alpha_2 tr(M\tilde{D})^2+\alpha_3 tr(M\tilde{D}^2)\right)^{\frac{1-n}{2n}}$$

with :

- $\alpha_1= \frac{1}{2\beta}$
- $\alpha_2=\frac{\gamma}{\beta}-1$
- $\alpha_3=1-\frac{1}{\beta}$
```

```{tab-item} Non Linear Equation 
$$S=\eta^\star\left(2\alpha_1 D + 2\alpha_2 M^D Tr(MD) + \alpha_3 (MD+DM)^D\right) $$

with $\eta^\star$ the apparent viscosity that depend of the strain rate $D$.

$$\eta^\star=2\eta_n \left(\alpha_1 tr(D^2) + \alpha_2 tr(MD)^2+\alpha_3 tr(MD^2)\right)^{\frac{1-n}{2n}}$$

with :

- $\alpha_1= \frac{1}{2\beta}$
- $\alpha_2=\frac{\gamma}{\beta}-1$
- $\alpha_3=1-\frac{1}{\beta}$
```

```{tab-item} Linear Equation 

$$S=\eta_1 \left(2\alpha_1 D + 2\alpha_2 M^D Tr(MD) + \alpha_3 (MD+DM)^D \right)$$

with :

- $\alpha_1= \frac{1}{\beta}$
- $\alpha_2=2\left(\frac{\gamma}{\beta}-1\right)$
- $\alpha_3=2\left(1-\frac{1}{\beta}\right)$
```
````




## Description of the orientation evolution equation
````{tab-set}
```{tab-item} Dimensionless equations
$$\dot{\tilde{c}}=\mathbf{\tilde{W}}(\tilde{u}).c-\lambda \left[\mathbf{\tilde{D}}(\tilde{u}).c-(c^T.\mathbf{\tilde{D}}(\tilde{u}).c).c \right] + \mathcal{Mo}  (c_0-c)$$

with

- $\mathcal{Mo}=\left(\frac{\eta_n}{\Sigma}\right)^{n}\frac{1}{\Gamma_{RX}}$

```

```{tab-item} None Dimensionless Equation
$$\dot{c}=\mathbf{W}.c-\lambda \left[\mathbf{D}.c-(c^T.\mathbf{D}.c).c \right]+\frac{1}{\Gamma_{RX}}(c_0-c)$$
```
````