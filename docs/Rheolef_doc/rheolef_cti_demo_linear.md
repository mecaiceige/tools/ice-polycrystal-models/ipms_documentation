# R3iCe

![GitLab tag (self-managed)](https://img.shields.io/gitlab/v/tag/mecaiceige/tools/ice-polycrystal-models/rheolef_cti?gitlab_url=https%3A%2F%2Fgricad-gitlab.univ-grenoble-alpes.fr&include_prereleases&sort=date)
[![PyPI version](https://img.shields.io/badge/license-GPLv3-blue.svg)](https://gricad-gitlab.univ-grenoble-alpes.fr/mecaiceige/tools/ice-polycrystal-models/rheolef_cti/-/blob/main/LICENSE)

::::{grid} 1 1 2 2
:class-container: text-center 
:gutter: 3


:::{grid-item-card} 
:link: rheolef_main
:link-type: doc
:class-header: bg-info

R3iCe formulation
^^^

:::



:::{grid-item-card} 
:link: rheolef_install
:link-type: doc
:class-header: bg-light

Install
^^^
:::

:::{grid-item-card} 
:link: rheolef_tutorial
:link-type: doc
:class-header: bg-light

Tutorial : run simulation
^^^

:::

:::{grid-item-card} 
:link: rheolef_output
:link-type: doc
:class-header: bg-light

Simulation output
^^^
:::

::::


# CTI demonstration


::::{grid} 1 1 1 2
:class-container: text-center 
:gutter: 3


:::{grid-item-card} 
:link: rheolef_cti_demo_linear
:link-type: doc
:class-header: .bg-warning

Linear CTI formulation
^^^

:::



:::{grid-item-card} 
:link: rheolef_cti_demo_non_linear
:link-type: doc
:class-header: .bg-body

Non-linear CTI formulation
^^^
:::

::::

The starting point of this demonstration is the theoretical work from {cite:t}`Boehler1987`. 

{cite:t}`Boehler1987` derives tensorial invariants, which serve as basis elements for constructing potentials that are linked to constitutive equations, using geometric arguments. He applies this method to three different types of symmetries: isotropic symmetry, continuous transverse isotropic symmetry, and orthotropic symmetry.

Continuous transverse isotropic symmetry can be considerated to model single crystal having an hexagonal symetry (ice, quartz, magnesium, ...) 

In the context of modeling single crystals with hexagonal symmetry, such as ice, quartz, and magnesium, we can employ continuous transverse isotropic symmetry. In this regard, we present a description of the potential and constitutive equations that govern this behavior, with the aim of establishing constitutive equations that relate the deviatoric strain rate $D$ to the deviatoric stress $S$.


```{figure} cti_geom.svg
---
height: 150px
name: directive-fig
---
Transverse isotropic representation. The vector $e_3$ gives the principal direction.
```

````{tab-set}
 ```{tab-item} Case $D=f(S)$ : $\phi_S^{(1)}$
In the case of continuous transverse isotropic (CTI) symmetry, it has been demonstrated by {cite:t}`Boehler1987` that any constitutive equation must be derived from a potential that is composed of the invariants of the deviatoric stress tensor $S$ :

$$\mathcal{B}_{CTI}=\left[tr(S^2),tr(S^3),tr(MS),tr(MS^2)\right]$$

with $M=e_3 \otimes e_3$

In order to obtain a linear constitutive behavior between $D$ and $S$ it is needed to have a quadratic potential $\phi_S^{(1)}$ as:

$$D=\left(\frac{\partial \phi_S^{(1)}}{\partial S} \right)^D$$

Therefore the potential $\phi_S^{(1)}$ can be written as :

$$ \phi_S^{(1)}=\delta_1 tr(S^2) + \delta_2 tr(MS)^2+\delta_3 tr(MS^2) $$

:::{tip}
Derivation rules:

$$\frac{\partial Tr(MS)}{\partial S}=M,\quad \frac{\partial Tr(S^2)}{\partial S}=2S,\quad \frac{\partial Tr(MS^2)}{\partial S}=MS+SM$$
:::

Using the derivation rules, we obtain:

$$ D=\left(\frac{\partial \phi_S^{(1)}}{\partial S}\right)^D=2\delta_1 S + 2\delta_2 M^D Tr(MS) + \delta_3 (MS+SM)^D $$

Three independent parameters need to be fitted from experimental data. The single crystal behavior can be describe as follow :

- $\psi_1$ is the fluidity for parallel shearing to the basal plane : 

$${}^{g}D_{13}=\frac{\psi_1}{2}{}^{g}S_{13},\quad{}^{g}D_{23}=\frac{\psi_1}{2}{}^{g}S_{23} $$

- $\beta$ is the viscosity ratio between shear parallel to the basal plane $\eta$ and the viscosity for shear within the basal plane : 

$$^{g}D_{12}=\beta\frac{\psi_1}{2}{}^{g}S_{12} $$

- $\gamma$ is the viscosity ratio between compression (or traction)
 along the $c$ axis $\eta_c$ and compression (or traction) in one direction ${}^ge_r$ within the basal plane : 

 $${}^{g}S_{33}=2\eta_c{}^{g}D_{33}, \quad \gamma {}^{g}S_{rr}=2\eta_c{}^{g}D_{rr}$$ 
 Therefore a same solicitation in different direction $S={}^gS_{33}={}^gS_{rr}$ we have : 
 
 $${}^gD_{rr}=\gamma{}^gD_{33} $$
 

Using this description, we found that [{bdg-primary}`identification demonstration`](rheolef_di_demo):

$$\delta_1=\frac{\psi_1\beta}{4}, \quad\delta_2=\frac{\psi_1}{2}\left(\beta\frac{\gamma+2}{4\gamma-1}-1\right), \quad\delta_3=\frac{\psi_1}{2}\left(1-\beta\right)$$

 ```

 ```{tab-item} Case $S=g(D)$ : $\phi_D^{(1)}$
 The inversion of the formulation is developed in Annex A from {cite:t}`Fabien2006`. Here we just summarized the potential and the constitutive equation.

 The potential is :

 $$\phi_D^{(1)}=\alpha_1 tr(D^2) + \alpha_2 tr(MD)^2+\alpha_3 tr(MD^2)$$

 The constitutive equation is : 
 
 $$S=\frac{\partial \phi_D^{(1)}}{\partial D}=2\alpha_1 D + 2\alpha_2 M^D Tr(MD) + \alpha_3 (MD+DM)^D$$

 The identification gives: 

 $$\alpha_1= \frac{\eta_1}{\beta},\quad \alpha_2=2\eta_1\left(\frac{\gamma}{\beta}-1\right),\quad \alpha_3=2\eta_1\left(1-\frac{1}{\beta}\right)$$
 
 with $\psi_1=\frac{1}{\eta_1}$
 ```
````

## References

```{bibliography}
:style: unsrt
```