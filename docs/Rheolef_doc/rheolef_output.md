# R3iCe

![GitLab tag (self-managed)](https://img.shields.io/gitlab/v/tag/mecaiceige/tools/ice-polycrystal-models/rheolef_cti?gitlab_url=https%3A%2F%2Fgricad-gitlab.univ-grenoble-alpes.fr&include_prereleases&sort=date)
[![PyPI version](https://img.shields.io/badge/license-GPLv3-blue.svg)](https://gricad-gitlab.univ-grenoble-alpes.fr/mecaiceige/tools/ice-polycrystal-models/rheolef_cti/-/blob/main/LICENSE)
::::{grid} 1 1 2 2
:class-container: text-center 
:gutter: 3


:::{grid-item-card} 
:link: rheolef_main
:link-type: doc
:class-header: bg-light

R3iCe formulation
^^^

:::



:::{grid-item-card} 
:link: rheolef_install
:link-type: doc
:class-header: bg-light

Install
^^^
:::

:::{grid-item-card} 
:link: rheolef_tutorial
:link-type: doc
:class-header: bg-light

Tutorial : run simulation
^^^

:::

:::{grid-item-card} 
:link: rheolef_output
:link-type: doc
:class-header: bg-info

Simulation output
^^^
:::

::::

## Output of `R3iCe` simulation

This page presents the output of the `R3iCe` simulation. It is composed of 2 types of file :

1. `*.txt.gz`, for compressed text file (unzip : `gzip -df *.txt.gz`)
2. `*.branch.gz`, for compressed vtk file (unzip `branch <name>.branch.gz -vtk`)

### Text files

#### `time_step.txt`

Each line corresponds to a new iteration time.

Columns :

1. `time_iter` : iteration time increment
2. `k_c_max` : number of iteration in the fix point P2
3. `k_nl_max` : number of iteration in the fix point P1
4. `residual_S_c` : $res_{c} = | S(c^{t+1,k_c+1},u^{t+1,k_c+1}) - S(c^{t+1,k_c},u^{t+1,k_c+1}) |$
5. `trD_mean` : average $tr(D)$
6. `trS_mean` : average $tr(S)$
7. `normD_mean` : average $norm(D)$
8. `normS_mean` : average $norm(S)$
9. `dij` : average of the deviatoric strain rate tensor component $D_{ij}$
10. `sij` : average of the stress tensor component $\sigma_{ij}$ ($\sigma=S+\frac{1}{d}ph.I$)
11. `runtime` : time in $ms$ of the iteration

#### `fix_point_nl.txt`

Each line corresponds to a fix point iteration for P1.

Columns :

1. `time_iter` : iteration time increment
2. `k_c` : iteration of the fix point P2
3. `k_nl` : iteration of the fix point P1
4. `residual_S_nl` : $res_{nl} = | S(c^{t+1,k_c},u^{t+1,k_c,k_{nl}+1}) - S(c^{t+1, k_c},u^{t+1,k_c,k_{nl}}) |$
5. `runtime_nl_loop`:  time in $ms$ of the iteration
6. `sopt_residue`:  residue of stokes solving problem from rheolef

#### `fix_point_c.txt`

Each line corresponds to a fix point iteration for P2.

Columns :

1. `time_iter` : iteration time increment
2. `k_c` : iteration of the fix point P2
3. `k_nl_max` : number of iteration of the fix point P1
4. `residual_S_c` : $res_{c} = | S(c^{t+1,k_c+1},u^{t+1,k_c+1}) - S(c^{t+1,k_c},u^{t+1,k_c+1}) |$
5. `runtime_c_loop`:  time in $ms$ of the iteration

### `*.branch.gz` files

#### `vector_P2.branch.gz`

This file contains the displacement vector field $u$ for each output step.

#### `vector_P0.branch.gz`

This file contains vector information for each output step :

1. `c-axis` : $c^t$ vector
2. `c0` : $c_0(u^t)$
3. `cRX` : $ \left( c_0(u^t) - c^t \right)$, correspond to the recrystallization vector of the time step $t$
4. `cRot` : $c^t-c^{t-1}$, correspond to the rotation vector between two time step

#### `tensor_P1.branch.gz`

This file contains the deviatoric strain rate tensor field $D$, the deviatoric stress tensor field $S$, and the stress tensor field $\sigma$ for each output step.

#### `tensor_P0.branch.gz`

This file contains the orientation tensor field $M=c \otimes c$ for each output step.

#### `scalar_P0.branch.gz`

This file contains scalar information for each output step :

1. `RX_speed` : $| \left( c_0(u^t) - c^t \right) |$, rate of recrystallization rotation
2. `eta_ice` : $\tilde{\eta}^\star$ value
3. `norm(D)` : norm of strain rate tensor $D$
4. `norm(S)` : norm of stress tensor $S$
5. `tr(D)` : trace of strain rate tensor $D$
6. `tr(S)` : trace of stress tensor $S$
7. `work_rate` : work rate D:S

#### `scalar_P1.branch.gz`

This file contains the pressure filed $ph$ for each output step.