# R3iCe

![GitLab tag (self-managed)](https://img.shields.io/gitlab/v/tag/mecaiceige/tools/ice-polycrystal-models/rheolef_cti?gitlab_url=https%3A%2F%2Fgricad-gitlab.univ-grenoble-alpes.fr&include_prereleases&sort=date)
[![PyPI version](https://img.shields.io/badge/license-GPLv3-blue.svg)](https://gricad-gitlab.univ-grenoble-alpes.fr/mecaiceige/tools/ice-polycrystal-models/rheolef_cti/-/blob/main/LICENSE)

::::{grid} 1 1 2 2
:class-container: text-center 
:gutter: 3


:::{grid-item-card} 
:link: rheolef_main
:link-type: doc
:class-header: bg-info

R3iCe formulation
^^^

:::



:::{grid-item-card} 
:link: rheolef_install
:link-type: doc
:class-header: bg-light

Install
^^^
:::

:::{grid-item-card} 
:link: rheolef_tutorial
:link-type: doc
:class-header: bg-light

Tutorial : run simulation
^^^

:::

:::{grid-item-card} 
:link: rheolef_output
:link-type: doc
:class-header: bg-light

Simulation output
^^^
:::

::::


# CTI demonstration


::::{grid} 1 1 1 2
:class-container: text-center 
:gutter: 3


:::{grid-item-card} 
:link: rheolef_cti_demo_linear
:link-type: doc
:class-header: .bg-body

Linear CTI formulation
^^^
:::

:::{grid-item-card} 
:link: rheolef_cti_demo_non_linear
:link-type: doc
:class-header: .bg-warning

Non-linear CTI formulation
^^^
:::

::::


The main tricks to develop the **non-linear** CTI formulation is to start from the linear one and to put the "same" potential at an higher order.


````{tab-set}
```{tab-item} Case $D=f(S^n)$ : $\phi_S^{(n)}$
**Linear formulation** can be rewrite as: 

$$ \phi_S^{(1)}=\frac{\psi_1}{2}\left(\delta_1 tr(S^2) + \delta_2 tr(MS)^2+\delta_3 tr(MS^2) \right)$$

with:

$$\delta_1=\frac{\beta}{2}, \quad\delta_2=\left(\beta\frac{\gamma+2}{4\gamma-1}-1\right), \quad\delta_3=\left(1-\beta\right)$$

**Non linear** potential is assume to be :

$$ \phi_S^{(n)}=\frac{\psi_n}{n+1}\left(\delta_1 tr(S^2) + \delta_2 tr(MS)^2+\delta_3 tr(MS^2) \right)^k$$

That can be written like:

$$ \phi_S^{(n)}=\frac{\psi_n}{n+1}\left( \tilde{\phi}_S^{(1)} \right)^k$$

Using the chain rule for deriving $\phi_S^{(n)}$ gives:

$$ D=\frac{\partial \phi_S^{(n)}}{\partial S}= \frac{\psi_n}{n+1} k \left(\tilde{\phi}_S^{(1)}\right)^{k-1} \frac{\partial \tilde{\phi}_S^{(1)}}{\partial S} $$


By construction, the non-linear relationship between $D$ and $S$ should behave as $D \sim S^n$, therefore:

$$ D \sim (S^2)^{k-1}\times S $$

By identification it gives:

$$ k =\frac{n+1}{2} $$

Putting everything together the non linear summarized as :

:::{important}
Potential :

$$ \phi_S^{(n)}=\frac{\psi_n}{n+1}\left(\delta_1 tr(S^2) + \delta_2 tr(MS)^2+\delta_3 tr(MS^2) \right)^{\frac{n+1}{2}}$$

Constitutive law :

$$ D=\psi_n^\star \left(2\alpha_1 D + 2\alpha_2 M^D Tr(MD) + \alpha_3 (MD+DM)^D \right)$$

with the effective fluidity :

$$\psi_n^\star = \psi_n\left(\delta_1 tr(S^2) + \delta_2 tr(MS)^2+\delta_3 tr(MS^2) \right)^{\frac{n-1}{2}}$$

and 

$$\delta_1=\frac{\beta}{2}, \quad\delta_2=\left(\beta\frac{\gamma+2}{4\gamma-1}-1\right), \quad\delta_3=\left(1-\beta\right)$$

:::



```

```{tab-item} Case $S=g(D^{\frac{1}{n}})$ : $\phi_D^{(n)}$
**Linear formulation** can be rewrite as: 

$$ \phi_D^{(1)}=2\eta_1 \left(\alpha_1 tr(D^2) + \alpha_2 tr(MD)^2+\alpha_3 tr(MD^2) \right)$$

with:

$$\alpha_1= \frac{1}{2\beta},\quad \alpha_2=\left(\frac{\gamma}{\beta}-1\right),\quad \alpha_3=\left(1-\frac{1}{\beta}\right)$$

and $\psi_1=\frac{1}{\eta_1}$

**Non linear** potential is assume to be :

$$ \phi_D^{(n)}=\frac{4n}{n+1}\eta_n \left(\alpha_1 tr(D^2) + \alpha2 tr(MD)^2+\alpha_3 tr(MD^2) \right)^k$$

That can be written like:

$$ \phi_D^{(n)}=\frac{4n}{n+1}\eta_n \left( \tilde{\phi}_D^{(1)} \right)^k$$

Using the chain rule for deriving $\phi_D^{(n)}$ gives:

$$ S=\frac{\partial \phi_D^{(n)}}{\partial D}= \frac{4n}{n+1}\eta_n k \left(\tilde{\phi}_D^{(1)}\right)^{k-1} \frac{\partial \tilde{\phi}_D^{(1)}}{\partial D} $$


By construction, the non-linear relationship between $D$ and $S$ should behave as $S \sim D^{\frac{1}{n}}$, therefore:

$$ S \sim (D^2)^{k-1}\times D $$

By identification it gives:

$$ k =\frac{n+1}{2n} $$

Putting everything together the non linear summarized as :

:::{important}
Potential :

$$ \phi_D^{(n)}= \frac{4n}{n+1} \left(\alpha_1 tr(D^2) + \alpha_2 tr(MD)^2+\alpha_3 tr(MD^2) \right)^{\frac{n+1}{2n}}$$

Constitutive law :

$$ S=\eta_n^\star \left(2\alpha_1 D + 2\alpha_2 M^D Tr(MD) + \alpha_3 (MD+DM)^D \right)$$

with the effective viscosity :

$$\eta_n^\star = 2\eta_n\left(\alpha_1 tr(D^2) + \alpha_2 tr(MD)^2+\alpha_3 tr(MD^2) \right)^{\frac{1-n}{2n}}$$

and 

$$\alpha_1= \frac{1}{2\beta},\quad \alpha_2=\left(\frac{\gamma}{\beta}-1\right),\quad \alpha_3=\left(1-\frac{1}{\beta}\right)$$
:::
```
````


:::{warning}
Both formulation $S=f(D)$ and $D=f(S)$ are identical with :

$$ \psi_n =\frac{1}{2^{n-1}\eta_n^n}$$
:::

:::{note}
Applying a shear stress parallel to the basal plane : ${}^gS=\begin{bmatrix}0 & {}^gS_{12} & 0 \\ {}^gS_{12} & 0 & 0 \\ 0 & 0 & 0 \end{bmatrix}$ gives :

$$ D_{12} = \frac{\psi_n}{2} \beta^{\frac{n+1}{2}} S_{12}^n$$

Therefore attention should be taken when going from linear to non-linear with this formulation the value of $\beta$ should be adapted. If $\beta$ value is kept the same as the linear one the effective enhancement factor $\beta^\star$ is :

$$ \beta^\star = \beta^{\frac{n+1}{2n}}$$
:::