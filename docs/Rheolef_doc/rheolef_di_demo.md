# R3iCe

![GitLab tag (self-managed)](https://img.shields.io/gitlab/v/tag/mecaiceige/tools/ice-polycrystal-models/rheolef_cti?gitlab_url=https%3A%2F%2Fgricad-gitlab.univ-grenoble-alpes.fr&include_prereleases&sort=date)
[![PyPI version](https://img.shields.io/badge/license-GPLv3-blue.svg)](https://gricad-gitlab.univ-grenoble-alpes.fr/mecaiceige/tools/ice-polycrystal-models/rheolef_cti/-/blob/main/LICENSE)

::::{grid} 1 1 2 2
:class-container: text-center 
:gutter: 3


:::{grid-item-card} 
:link: rheolef_main
:link-type: doc
:class-header: bg-info

R3iCe formulation
^^^

:::



:::{grid-item-card} 
:link: rheolef_install
:link-type: doc
:class-header: bg-light

Install
^^^
:::

:::{grid-item-card} 
:link: rheolef_tutorial
:link-type: doc
:class-header: bg-light

Tutorial : run simulation
^^^

:::

:::{grid-item-card} 
:link: rheolef_output
:link-type: doc
:class-header: bg-light

Simulation output
^^^
:::

::::


# CTI demonstration


::::{grid} 1 1 1 2
:class-container: text-center 
:gutter: 3


:::{grid-item-card} 
:link: rheolef_cti_demo_linear
:link-type: doc
:class-header: .bg-warning

Linear CTI formulation
^^^

:::



:::{grid-item-card} 
:link: rheolef_cti_demo_non_linear
:link-type: doc
:class-header: .bg-body

Non-linear CTI formulation
^^^
:::

::::


## Demonstration of $\delta_i$ identification

### Constitutive equation CTI and single crystal behavior

The constitutive equation is:

$$ D=\left(\frac{\partial \phi_S^{(1)}}{\partial S}\right)^D=2\delta_1 S + 2\delta_2 M^D Tr(MS) + \delta_3 (MS+SM)^D $$

The single crystal behavior can be describe as :

- $\psi_1$ is the fluidity for parallel shearing to the basal plane : 

$${}^{g}D_{13}=\frac{\psi_1}{2}{}^{g}S_{13},\quad{}^{g}D_{23}=\frac{\psi_1}{2}{}^{g}S_{23} $$

- $\beta$ is the viscosity ratio between shear parallel to the basal plane $\eta$ and the viscosity for shear within the basal plane : 

$$^{g}D_{12}=\beta\frac{\psi_1}{2}{}^{g}S_{12} $$

- $\gamma$ is the viscosity ratio between compression (or traction)
 along the $c$ axis $\eta_c$ and compression (or traction) in one direction ${}^ge_r$ within the basal plane : 

 $${}^{g}S_{33}=2\eta_c{}^{g}D_{33}, \quad \gamma {}^{g}S_{rr}=2\eta_c{}^{g}D_{rr}$$ 
 Therefore a same solicitation in different direction $S={}^gS_{33}={}^gS_{rr}$ we have : 
 
 $${}^gD_{rr}=\gamma{}^gD_{33} $$
 

### $\delta_i$ identification


In order to identify the parameter $(\delta_1,\delta_2,\delta_3)$ from the constitutive equation, we developed it and compare to the single crystal behavior.

:::{note}
With $v3=(0,0,1)$

$$M=\begin{bmatrix}
    0 & 0 & 0 \\
    0 & 0 & 0 \\
    0 & 0 & 1
\end{bmatrix}, \quad M^D=\frac{1}{3}\begin{bmatrix}
    -1 & 0 & 0 \\
    0 & -1 & 0 \\
    0 & 0 & 2
\end{bmatrix}, \quad Tr(M{}^gD)={}^gD_{33}$$
:::


1. $\delta_1$ is given by applying a stress ${}^gS=\begin{bmatrix}0 & {}^gS_{12} & 0 \\ {}^gS_{12} & 0 & 0 \\ 0 & 0 & 0 \end{bmatrix}$

Then 

$$Tr(MS)=0, \quad \left(MS+SM\right)^D=0$$

The constitutive equation gives :

$${}^gD_{12}=2\delta_1{}^gS_{12}$$ 

Therefore :

$$\delta_1=\frac{\psi_1\beta}{4}$$

2. $\delta_3$ is given by applying a stress ${}^gS=\begin{bmatrix}0 & 0 & {}^gS_{13} \\  0 & 0 & 0 \\ {}^gS_{13} & 0 & 0 \end{bmatrix}$ : 

Then 

$$Tr(MS)=0, \quad \left(MS+SM\right)^D=S$$

The constitutive equation gives :

$${}^gD_{13}=(2\delta_1+\delta_3){}^gS_{13} $$ 

Therefore 

$$\delta_3=\frac{\psi_1}{2}\left(1-\beta\right)$$


3. $\delta_2$ identification is less trivial therefore I try to walk you off all the steps.
    - Let's apply a uniaxial stress $\tilde{\sigma}$ along the $c$ axis. Therefore the stress tensor is ${}^g\sigma=\begin{bmatrix}0&0&0\\0&0&0\\0&0&\tilde{\sigma} \end{bmatrix}$ and the deviatoric stress tensor associated is ${}^gS=\begin{bmatrix}-\frac{1}{3}\tilde{\sigma}&0&0\\0&-\frac{1}{3}\tilde{\sigma}&0\\0&0&\frac{2}{3}\tilde{\sigma} \end{bmatrix}$
        
        - $Tr(M{}^gS)=\frac{2\tilde{\sigma}}{3}$
        - $\left(M{}^gS+{}^gSM\right)^D= \begin{bmatrix}0&0&0\\0&0&0\\0&0&\frac{4}{3}\tilde{\sigma} \end{bmatrix}^D=\begin{bmatrix}-\frac{4}{9}\tilde{\sigma}&0&0\\0&-\frac{4}{9}\tilde{\sigma}&0\\0&0&\frac{8}{9}\tilde{\sigma} \end{bmatrix}$
        - using the constitutive equation, we obtain for ${}^gD_{33}$ : 
        
        $${}^gD_{33}=\delta_1\frac{4}{3}\tilde{\sigma}+\delta_2\frac{8}{9}\tilde{\sigma}+\delta_3 \frac{8}{9}\tilde{\sigma}$$

    - Now let's apply a uniaxial stress $\tilde{\sigma}$ along an axis perpendicular to the $c$ axis for instance the direction $1$. Therefore the stress tensor is ${}^g\sigma=\begin{bmatrix}\tilde{\sigma}&0&0\\0&0&0\\0&0&0 \end{bmatrix}$ and the deviatoric stress tensor associated is ${}^gS=\begin{bmatrix}\frac{2}{3}\tilde{\sigma}&0&0\\0&-\frac{1}{3}\tilde{\sigma}&0\\0&0&-\frac{1}{3}\tilde{\sigma} \end{bmatrix}$
        
        - $Tr(M{}^gS)=-\frac{\tilde{\sigma}}{3}$
        - $\left(M{}^gS+{}^gSM\right)^D= \begin{bmatrix}0&0&0\\0&0&0\\0&0&-\frac{2}{3}\tilde{\sigma} \end{bmatrix}^D=\begin{bmatrix}\frac{2}{9}\tilde{\sigma}&0&0\\0&\frac{2}{9}\tilde{\sigma}&0\\0&0&\frac{4}{9}\tilde{\sigma} \end{bmatrix}$
        - using the constitutive equation, we obtain for ${}^gD_{11}$ : 
        
        $${}^gD_{11}=\delta_1\frac{4}{3}\tilde{\sigma}+\delta_2\frac{2}{9}\tilde{\sigma}+\delta_3 \frac{2}{9}\tilde{\sigma}$$
    
    - Finally from the grain behavior we can write that $\gamma {}^gD_{33}={}^gD_{11}$ : 
    
    $$ \gamma\left(\frac{4}{3}\delta_1+\frac{8}{9}\delta_2+\frac{8}{9}\delta_3\right)=\frac{4}{3}\delta_1+\frac{2}{9}\delta_2+\frac{2}{9}\delta_3$$
    
    - It gives : 
    
    $$\delta_2=6\delta_1\frac{1-\gamma}{4\gamma-1}-\delta_3 $$
    
    - Using $\delta_1$ and $\delta_3$ found above it gives :
    
    $$\delta_2=\frac{\psi_1}{2}\left(\beta\frac{\gamma+2}{4\gamma-1}-1\right)$$
