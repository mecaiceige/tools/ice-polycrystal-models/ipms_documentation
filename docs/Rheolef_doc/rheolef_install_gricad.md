# R3iCe

![GitLab tag (self-managed)](https://img.shields.io/gitlab/v/tag/mecaiceige/tools/ice-polycrystal-models/rheolef_cti?gitlab_url=https%3A%2F%2Fgricad-gitlab.univ-grenoble-alpes.fr&include_prereleases&sort=date)
[![PyPI version](https://img.shields.io/badge/license-GPLv3-blue.svg)](https://gricad-gitlab.univ-grenoble-alpes.fr/mecaiceige/tools/ice-polycrystal-models/rheolef_cti/-/blob/main/LICENSE)

::::{grid} 1 1 2 2
:class-container: text-center 
:gutter: 3


:::{grid-item-card} 
:link: rheolef_main
:link-type: doc
:class-header: bg-light

R3iCe formulation
^^^

:::



:::{grid-item-card} 
:link: rheolef_install
:link-type: doc
:class-header: bg-info

Install
^^^
:::

:::{grid-item-card} 
:link: rheolef_tutorial
:link-type: doc
:class-header: bg-light

Tutorial : run simulation
^^^

:::

:::{grid-item-card} 
:link: rheolef_output
:link-type: doc
:class-header: bg-light

Simulation output
^^^
:::

::::

## Using guix repository

### Install `rheolef`

```{warning}
04/17/2023 : Everything seems to compile correctly but the code seems to not run in parallel.
```

First of all you need to install guix repository with the recipe for installing rheolef library [git](https://gricad-gitlab.univ-grenoble-alpes.fr/scalde-contrib/ljk-guix-channel)

1. Start a guix session
    ```shell
    source /applis/site/guix-start.sh
    ```

2. Edit `.config/guix/channels.scm` to add the repository
    ```
    ;; Add my personal packages to those Guix provides.
    (cons*
    (channel
    (name 'ljk-guix)
    (url "https://gricad-gitlab.univ-grenoble-alpes.fr/scalde-contrib/ljk-guix-channel.git"))
    (channel
    (name 'gricad-guix-packages)
    (url "https://gricad-gitlab.univ-grenoble-alpes.fr/bouttiep/gricad_guix_packages.git"))
    %default-channels)
    ```

3. Create a guix environnement to install dependencies

    ```shell
    guix pull
    guix install -p $GUIX_USER_PROFILE_DIR/rheolef_mpi rheolef-parallel gmsh gcc-toolchain@12.2.0
    ```

4. Download [`json`](https://github.com/nlohmann/json) library
    ```shell
    git clone https://github.com/nlohmann/json
    ```

### Compile [`R3iCe`](https://gricad-gitlab.univ-grenoble-alpes.fr/mecaiceige/tools/ice-polycrystal-models/rheolef_cti)

1. Clone the repository 
    ```shell
    git clone https://gricad-gitlab.univ-grenoble-alpes.fr/mecaiceige/tools/ice-polycrystal-models/rheolef_cti
    ```

2. Change `Makefile`
    ```shell
    cd rheolef_cti
    cp Makefile_gricad Makefile
    ``` 

3. Edit `Makefile` : change the `/path/to/lib/json/` to the `include` folder path of the json library download above.

4. Compile the `R3iCe` function:
    ```shell
    refresh_guix rheolef_mpi
    make
    mkdir bin
    mv src/R3iCe bin/
    ```

5. Add `bin` folder to your `.bashrc` file
   ```
    export PATH="/myPATH/rheolef_cti/bin:$PATH"
   ```

### Run the program

The `R3iCe` function should work from everywhere, do not forget to load the guix environment before running it.

```shell
source /applis/site/guix-start.sh
refresh_guix rheolef_mpi
R3iCe -v
```

## Using docker image

On gricad I recommend using [`charliecloud`](https://hpc.github.io/charliecloud/index.html) container.

### Compile [`charliecloud`](https://hpc.github.io/charliecloud/index.html)

I recommend to compile [`charliecloud`](https://hpc.github.io/charliecloud/index.html) as no version is available using [guix](https://packages.guix.gnu.org/) and only the version 0.24 is available using [nix](https://search.nixos.org/packages) at this date (29/04/2023).

I tested the [install](https://hpc.github.io/charliecloud/install.html) with [`charliecloud 0.32`](https://github.com/hpc/charliecloud/releases/tag/v0.32)

```shell
source /applis/site/guix-start.sh
guix install -p $GUIX_USER_PROFILE_DIR/charliecloud gcc-toolchain
refresh_guix charliecloud
```
An then follow the protocol given in [charliecloud install](https://hpc.github.io/charliecloud/install.html)

You will not be able to run `sudo make install` on gricad therefore add the `bin/` folder to your `.bashrc`

```
export PATH="/bettik/PROJECTS/pr-rheolefice/Software/charliecloud-0.32/bin:$PATH"
```

### Convert your `docker` to [`charliecloud`](https://hpc.github.io/charliecloud/index.html)

[`charliecloud`](https://hpc.github.io/charliecloud/index.html) is an other container that can be used on hpc like gricad as it does not required root access. To convert the docker image already installed to charliecload image run :

```shell
ch-convert -i docker rheolef_cti:latest ch_cti.tar.gz
```

Move the compressed file on the hcp you when to use and uzip it.

```shell
tar -xf ch_cti.tar.gz -C output/directory/
```

### Run a simulation

Then to submit a simulation here is an example of an `oar` file:

````{card} 
run.oar
^^^

```bash
#!/bin/bash
#OAR -n rh_gricad_3d
#OAR -O rh_gricad_3d.%jobid%.o
#OAR -E rh_gricad_3d.%jobid%.e
#OAR --project rheolefice
#OAR -l nodes=1/core=16,walltime=00:30:00

# nb of core
nb_core=16

# charliecloud image path
path_img=/bettik/PROJECTS/pr-rheolefice/Software/ch_cti/
# path simu
path_simu=/home/chauvet/gricad/run_16/


time ch-run --home --cd $path_simu $path_img -- mpirun -n $nb_core R3iCe -j metadata.json 2> /dev/null

```
````

The folder `$path_simu` should contain `metadata.json` file.

Then submit the run :

```shell
oarsub -S ./run.oar
```
