# R3iCe

![GitLab tag (self-managed)](https://img.shields.io/gitlab/v/tag/mecaiceige/tools/ice-polycrystal-models/rheolef_cti?gitlab_url=https%3A%2F%2Fgricad-gitlab.univ-grenoble-alpes.fr&include_prereleases&sort=date)
[![PyPI version](https://img.shields.io/badge/license-GPLv3-blue.svg)](https://gricad-gitlab.univ-grenoble-alpes.fr/mecaiceige/tools/ice-polycrystal-models/rheolef_cti/-/blob/main/LICENSE)

::::{grid} 1 1 2 2
:class-container: text-center 
:gutter: 3


:::{grid-item-card} 
:link: rheolef_main
:link-type: doc
:class-header: bg-light

R3iCe formulation
^^^

:::



:::{grid-item-card} 
:link: rheolef_install
:link-type: doc
:class-header: bg-info

Install
^^^
:::

:::{grid-item-card} 
:link: rheolef_tutorial
:link-type: doc
:class-header: bg-light

Tutorial : run simulation
^^^

:::

:::{grid-item-card} 
:link: rheolef_output
:link-type: doc
:class-header: bg-light

Simulation output
^^^
:::

::::

# Installation

This page present how to compile the code on a linux OS or how to use a docker container. If you want to install it on [gricad](https://gricad-doc.univ-grenoble-alpes.fr/) using a [guix](https://guix.gnu.org/) or [charliecloud](https://hpc.github.io/charliecloud/index.html) check [{bdg-primary}`gricad install`](rheolef_install_gricad)

## Using Docker image

If you prefer to work within a [Docker](https://www.docker.com/) container you can use the [Dockerfile](https://gricad-gitlab.univ-grenoble-alpes.fr/mecaiceige/tools/ice-polycrystal-models/rheolef_cti/-/blob/main/docker/debian_bookworm/Dockerfile).

### Build docker image

```shell
wget https://gricad-gitlab.univ-grenoble-alpes.fr/mecaiceige/tools/ice-polycrystal-models/rheolef_cti/-/raw/main/docker/debian_bookworm/Dockerfile
docker build -t rheolef_cti .
```

### Run docker container

To run a container being `user_host` user on the host machine :

```shell
docker run -u $(id -u user_host):$(id -g user_host) -v /host/:/home/cti_user/ -it rheolef_cti
```

- `-v` : enable to mount `/host/` folder for the host machine to `/home/cti_user/` folder in the container
- `-u` : can be necessary in order to the container to be able to write in the folder

## Compile and Install `R3iCe` without docker

### Dependencies

#### `rheolef`

Install `rheolef` librairy. See this [documentation pages](https://membres-ljk.imag.fr/Pierre.Saramito/rheolef/html/download_page.html) to install it. The `R3iCe` function as been tested using rheolef 7.2.

#### `nlohmann/json`

Install `nlohmann/json` librairy [git](https://github.com/nlohmann/json).

On linux distribution this can be done with:

```shell
git clone https://github.com/nlohmann/json
cp -r json/include/nlohmann/ /usr/lib/x86_64-linux-gnu/
```

:::{tip}
If you cannot put where you want the json library you can edit the [`Makefile`](https://gricad-gitlab.univ-grenoble-alpes.fr/mecaiceige/tools/ice-polycrystal-models/rheolef_cti/-/blob/main/Makefile) and add to the variable `CXXFLAGS` a local folder by adding at the end of the line :

```shell
-I/mypath/json/include
```
:::

### Compilation

Then you need to compile the code from this [git repository](https://gricad-gitlab.univ-grenoble-alpes.fr/mecaiceige/tools/ice-polycrystal-models/rheolef_cti).


```shell
git clone https://gricad-gitlab.univ-grenoble-alpes.fr/mecaiceige/tools/ice-polycrystal-models/rheolef_cti
cd rheolef_cti/
make
```

If you don't get error it means you successfully compile the `R3iCe` binary and it is available in `bin/R3iCe`.

### Install

If you have admin right you can run :

```shell
sudo make install
```

Overwise in order to be able to run `R3iCe` code from anywhere you can add this line to your `.bashrc` file.

```
export PATH="(PATH_TO_FOLDER)/rheolef_cti/bin:$PATH"
```