# R3iCe

![GitLab tag (self-managed)](https://img.shields.io/gitlab/v/tag/mecaiceige/tools/ice-polycrystal-models/rheolef_cti?gitlab_url=https%3A%2F%2Fgricad-gitlab.univ-grenoble-alpes.fr&include_prereleases&sort=date)
[![PyPI version](https://img.shields.io/badge/license-GPLv3-blue.svg)](https://gricad-gitlab.univ-grenoble-alpes.fr/mecaiceige/tools/ice-polycrystal-models/rheolef_cti/-/blob/main/LICENSE)

::::{grid} 1 1 2 2
:class-container: text-center 
:gutter: 3


:::{grid-item-card} 
:link: rheolef_main
:link-type: doc
:class-header: bg-info

R3iCe formulation
^^^

:::



:::{grid-item-card} 
:link: rheolef_install
:link-type: doc
:class-header: bg-light

Install
^^^
:::

:::{grid-item-card} 
:link: rheolef_tutorial
:link-type: doc
:class-header: bg-light

Tutorial : run simulation
^^^

:::

:::{grid-item-card} 
:link: rheolef_output
:link-type: doc
:class-header: bg-light

Simulation output
^^^
:::

::::

# R3iCe formulation

::::{grid} 1 1 1 2
:class-container: text-center 
:gutter: 3


:::{grid-item-card} 
:link: rheolef_main
:link-type: doc
:class-header: .bg-body

R3iCe formulation
^^^

:::



:::{grid-item-card} 
:link: rheolef_numeric
:link-type: doc
:class-header: .bg-warning

Numerical implementation
^^^
:::

::::


## Description of the numerical algorithm

### System of equation
 
````{card}
P1 i.
^^^

$$\tilde{S}=\tilde{\eta}^\star\left(2\alpha_1 \tilde{D} + 2\alpha_2 M^D Tr(M\tilde{D}) + \alpha_3 (M\tilde{D}+\tilde{D}M)^D\right)$$

where

$$\tilde{\eta}^\star=2\left(\alpha_1 tr(\tilde{D}^2) + \alpha_2 tr(M\tilde{D})^2+\alpha_3 tr(M\tilde{D}^2)\right)^{\frac{1-n}{2n}}$$
````
````{card}
P1 ii.
^^^

$$-\tilde{\nabla}\tilde{S}+\tilde{\nabla}\tilde{p}=0$$
````
````{card}
P2.
^^^

$$\dot{\tilde{c}}=\mathbf{\tilde{W}}(\tilde{u}).c-\lambda \left[\mathbf{\tilde{D}}(\tilde{u}).c-(c^T.\mathbf{\tilde{D}}(\tilde{u}).c).c \right] + \mathcal{Mo}  (c_0-c)$$

````

### Time discretization

The time discretization is using a [BDF2 differentiation formula](https://en.wikipedia.org/wiki/Backward_differentiation_formula) where $\Delta \tilde{t}$ is the discrete time step.

````{card}
P1 i.
^^^

$$\tilde{S}^{t+1}={\tilde{\eta}^{t+1}}^\star\left(2\alpha_1 \tilde{D}(u^{t+1}) + 2\alpha_2 {M^{t+1}}^D Tr(M^{t+1}\tilde{D}(u^{t+1})) + \alpha_3 (M^{t+1}\tilde{D}(u^{t+1})+\tilde{D}(u^{t+1})M^{t+1})^D\right)$$

where

$${\tilde{\eta}^{t+1}}^\star = 2\left(\alpha_1 tr({\tilde{D}(u^{t+1})}^2) + \alpha_2 tr(M^{t+1}\tilde{D}(u^{t+1}))^2+\alpha_3 tr(M^{t+1}{\tilde{D}(u^{t+1})}^2)\right)^{\frac{1-n}{2n}}$$
````
````{card}
P1 ii.
^^^

$$-\tilde{\nabla}\tilde{S}^{t+1}+\tilde{\nabla}\tilde{p}^{t+1}=0$$
````
````{card}
P2.
^^^

$$\frac{1}{\Delta \tilde{t}}\left(c^{t+1}-\frac{4}{3}c^t+\frac{1}{3}c^{t-1}\right)=\frac{2}{3}\left(\mathbf{\tilde{W}}(\tilde{u}^{t+1}).c^{t+1}-\lambda \left[\mathbf{\tilde{D}}(\tilde{u}^{t+1}).c^{t+1}-({c^{t+1}}^T.\mathbf{\tilde{D}}(\tilde{u^{t+1}}).c^{t+1}).c^{t+1} \right] + \mathcal{Mo}  (c_0^{t+1}-c^{t+1})\right)$$

````

### Numerical algorithm

The numerical algorithm is based on using two nested fixed loops. The first loop solves the non-linear formulation of the CTI law, while the second loop is used for the time step. To ensure the algorithm converges quickly, it is recommended to limit the maximum number of iterations for the non-linear fixed loop to one. The fixed point is included in the code to enable R3iCe simulation with non-linear CTI without time evolution.

`````````{card}
main program
^^^
**initialization**
 
- generate $c^0$
- $c^{-1}=c^{-2}=c^{-0}$
- $u^0=0$
- $p^0=0$

 ````````{card}
 time loop : find $c^{t+1},~u^{t+1},~p^{t+1}$ knowing $c^{t},~c^ {t-1}$
 ^^^
  ```````{card}
  fix point to solve P2 $k_c$

  **initialization**
  
  - $c^{t+1,k_c=0}=c^{t}$ 
  - $u^{t+1,k_c=0}=c^{t}$
  ^^^
   **Fix point to converge to $c^{t+1}$ and $u^{t+1}$**
   ``````{card}
   fix point to solve non linear P1 $k_{nl}$

   **initialization**

   - $u^{t+1,k_c,k_{nl}=0}=u^{t+1,k_c}$
   ^^^
    **Fix point to converge to $u^{t+1,k_c}$ with $c^{t+1,k_c}$ fix**
    `````{card}
    rheolef magic solve linearized P1
    ^^^
    Find $u^{t+1,k_c,k_{nl}+1}$ solving P1 with
    - $c^{t+1,k_c}$
    - ${\tilde{\eta}^\star}^{t+1,k_c,k_{nl}}(c^{t+1,k_c},u^{t+1,k_c,k_{nl}})$

    `````
   Update :

   - ${\tilde{\eta}^\star}^{t+1,k_c,k_{nl}+1}(c^{t+1,k_c},u^{t+1,k_c,k_{nl}+1})$

   Compute residual :

   $$res_{nl} = | S(c^{t+1,k_c},u^{t+1,k_c,k_{nl}+1}) - S(c^{t+1, k_c},u^{t+1,k_c,k_{nl}}) |$$

   +++
   if $res_{nl}<tol_{nl}$ or $k_{nl}> it\_max\_nl$ : STOP

   $u^{t+1,k_c+1}=u^{t+1,k_c,k_{nl}}$

   ``````
  Find $c_0(u^{t+1,k_c+1})$ and compute :

  $c^{t+1,k_c+1}(u^{t+1,k_c},c^{t},c^{t-1},c_0)$ using P2

  Compute residual :

  $$res_{c} = | S(c^{t+1,k_c+1},u^{t+1,k_c+1}) - S(c^{t+1,k_c},u^{t+1,k_c+1}) |$$

  +++
  if $res_{c}<tol_{c}$ or $k_{c} > it\_max\_c$ : STOP

  $u^{t+1,k_c+1}=u^{t+1,k_c,k_{nl}}$

  ```````
 **convergence successful**

 $c^{t+1}$ and $u^{t+1}$ found
 +++ 
 do $nb\_t$ time loop
 ````````
`````````