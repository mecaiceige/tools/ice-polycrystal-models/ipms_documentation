# Orientation Generator

The generation of orientation is based on [MTEX](https://mtex-toolbox.github.io/) and therefore [Matlab](https://fr.mathworks.com/products/matlab.html).

The script has been tested with [MTEX 5.10.0](https://github.com/mtex-toolbox/mtex/releases/tag/mtex-5.10.0) and [MatlabR2021a](https://fr.mathworks.com/products/new_products/release2021a.html) but is likely to works with newer version.

## Install

1. Install [MTEX](https://mtex-toolbox.github.io/) in Matlab.

2. Install the matlab functions from [`orientation_tools`](https://gricad-gitlab.univ-grenoble-alpes.fr/mecaiceige/tools/ice-polycrystal-models/orientation_tools).

```shell
git clone https://gricad-gitlab.univ-grenoble-alpes.fr/mecaiceige/tools/ice-polycrystal-models/orientation_tools
```

And add to your Matlab `Set-Path` the folder [`orientation_tools/src`](https://gricad-gitlab.univ-grenoble-alpes.fr/mecaiceige/tools/ice-polycrystal-models/orientation_tools/-/tree/main/src)


## Creating orientation file for [`R³iCe`](https://gricad-gitlab.univ-grenoble-alpes.fr/mecaiceige/tools/ice-polycrystal-models/rheolef_cti) and craft

Open the [`Orientation_Generator.m`](https://gricad-gitlab.univ-grenoble-alpes.fr/mecaiceige/tools/ice-polycrystal-models/orientation_tools/-/blob/main/src/Orientation_Generator.m) script.

The only parameters that needs to be change in this script are in this paragraph.

```matlab
%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Choose the type of texture that you want to build
%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% number of orientation wanted in the output file
nb_ori=200;
% Select type of ODF ('uniform' or 'singleMax' or 'girdle')
prefix='uniform';
% angles spread for singleMax an girdle (degree)
aS=10;
% angles between y axis and girdle center (degree)
aG=45;

%% --------------------------------------------------
%% ------ NOTHINHG TO CHANGE BELLOW THIS LINE -------
%% --------------------------------------------------

```

Option :
1. `nb_ori` : is the number of orientation that you want to generate
2. `prefix` : is the type of texture from which you want to generate the orientation (`uniform`, `singleMax`, `girdle`)

:::{note}
If `uniform` is selected it generates a uniform texture.
:::

:::{note}
If `singleMax` is selected it generates a texture with a single maximum align with **y**-axis. 

- `aS` prescribe the opening of the single maximum (angle in degree).
:::

:::{note}
If `girdle` is selected it generates a "girdle" like texture. 

- `aG` prescribe the angle between the girdle and the **y**-axis (in degree).
- `aS` prescribe the spreading around the girdle (angle in degree).
:::

Once the input parameters are set you can run the script.

## Output

The outputs are generated in a new folder :

1. `*.phase` file contain `nb_ori` orientations to be used in `craft`
2. `*.rhori` file contain **the same** `nb_ori` orientations to be used in `rheolef_cti`
3. `*_ori.png` image is the texture of the `nb_ori` orientations picked
4. `*_ori.th` image is the texture **from which** the `nb_ori` orientations are picked