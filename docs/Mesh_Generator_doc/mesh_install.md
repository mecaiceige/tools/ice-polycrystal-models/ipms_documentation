# Mesh generator

::::{grid} 1 1 1 2
:class-container: text-center 
:gutter: 3


:::{grid-item-card} 
:link: mesh_main
:link-type: doc
:class-header: bg-light

Quick start
^^^

:::


:::{grid-item-card} 
:link: mesh_install
:link-type: doc
:class-header: bg-info

Install
^^^
:::
::::

## Installation

### `neper`

Install [Neper](https://neper.info/index.html). Neper is used to generate Voronoi microstructure.

### `rheolef`

Install `rheolef` librairy. See this [documentation pages](https://membres-ljk.imag.fr/Pierre.Saramito/rheolef/html/download_page.html) to install it.

### Scripts and Functions

[`mesh_tools`](https://gricad-gitlab.univ-grenoble-alpes.fr/mecaiceige/tools/ice-polycrystal-models/mesh_tools) consists of a set of bash scripts and Python functions. To install it, follow these steps:

1. Clone the repository:

    ```shell
    git clone https://gricad-gitlab.univ-grenoble-alpes.fr/mecaiceige/tools/ice-polycrystal-models/mesh_tools
    ```

2. Navigate to the `mesh_tools` directory:

    ```shell
    cd mesh_tools
    ```

3. Install the required Python dependencies:

    ```shell
    pip install -r requirements.txt
    ```

If you want to be able to run the functions from anywhere, you can add the following line to your `.bashrc` file:

```shell
export PATH="/mypath/mesh_tools/src/Neper_Tesselation:$PATH"
```

This will allow you to execute the functions by simply typing their names in the terminal, without having to navigate to the `mesh_tools` directory first.