# Mesh generator

::::{grid} 1 1 1 2
:class-container: text-center 
:gutter: 3


:::{grid-item-card} 
:link: mesh_main
:link-type: doc
:class-header: bg-info

Quick start
^^^

:::


:::{grid-item-card} 
:link: mesh_install
:link-type: doc
:class-header: bg-light

Install
^^^
:::
::::


## Quick Start

If you want to perform simulations on `craft` and `R3iCe` using the same initial microstructure, you can use the `NT_3D.sh` or `NT_2D5.sh` functions to generate microstructure files from the same Voronoi microstructure generated with `neper`.

### Geometry

The geometry of the sample is a cuboid with dimensions that depend on the aspect ratio $\lambda = \frac{z}{x}$. Specifically, the dimensions are given by:

$$
[x, y, z] = \left[ \frac{\sqrt{\lambda}}{\lambda}, 2 \max\left(\frac{\sqrt{\lambda}}{\lambda}, \sqrt{\lambda}\right), \sqrt{\lambda} \right]
$$

This ensures that the surface area $n_y$ is equal to 1.


### Generate Voronoi microstructure

#### 3D

```shell
# number of grains in the microstructure
nb_g=100
# aspect ratio x/z
ar=0.1
# file name
fn=n100_ar10

# generate the microstructure
NT_3D.sh $nb_g $ar $fn
```

#### 2D-1/2

```shell
# number of grains in the microstructure
nb_g=250
# file name
fn=n250_2d5

# generate the microstructure
NT_2D5.sh $nb_g $ar $fn
```

#### Output

Three output are given in folder `n100_ar10/`:

   1. `n100_ar10_craft.vtk` for `craft` simulation. It can be visualized using `paraview` 
        
        ```shell 
        paraview n100_ar10/n100_ar10_craft.vtk
        ```

   2. `n100_ar10_gmsh.msh`. It can be visualized using `gmsh`:
        
        ```shell 
        gmsh n100_ar10/n100_ar10_gsmh.msh
        ```
        
   3. `n100_ar10_rheolef.geo` that correspond to the `n100_ar10_gmsh.msh` but compatible with [`R3iCe`](https://membres-ljk.imag.fr/Pierre.Saramito/rheolef/html/index.html) library and with the code [`R3iCe`](https://gricad-gitlab.univ-grenoble-alpes.fr/mecaiceige/tools/ice-polycrystal-models/rheolef_cti)

### Generate mesh file from AITA measurement

```{note}
AITA measurement are raster measurement of orientation. Therefore the code can be adapted is you have an other type of raster measurement (EBSD, picture,...). Feel free to dig into the code to do it or contact me if you need help.
```

```{warning}
During the generation of mesh from AITA measurement the grain are labelled $Gi$. But during the meshing some small grains can disappear. There the orientation file `*.rhori` should be edited and the lines containing the orientations of the missing grains removed. Overwise an error will appear when running `CTI` program.
```

#### Generate 3d mesh

##### Export file using [`xarrayaita`](https://gricad-gitlab.univ-grenoble-alpes.fr/mecaiceige/tools/lib_python/xarray_aita)

Here is an example of python script to run in order to obtain `craft` input file and `neper` compatible file `.tesr`.

```python
import xarrayaita.loadData_aita as lda
import xarrayaita.aita as xa
import numpy as np

adr_dat='/data/Manips/Columnar_Ice/CI02/DataBrut/AITA/CI02_aita.dat'
adr_micro='/data/Manips/Columnar_Ice/CI02/DataBrut/AITA/CI02_micro.bmp'

ds=lda.aita5col(adr_dat,adr_micro)

pos=[[1.4,75],[8,89.6]]
ds=ds.aita.crop(pos)
ds=ds.aita.rot180()

ds.aita.craft('CI02_3d',res=0.5,m3d=10,tesr=True)
```

##### Generate mesh from `.tesr` file

Run

```shell
mesh_size = 0.3
output=CI02_3d
input_tesr=CI02_3d_micro.tesr

NT_AITA.sh $input_tesr $output $mesh_size
```

##### Output

Two output are given in folder `CI02/`:

   1. `CI02_gmsh.msh`. It can be visualized using `gmsh`:
        
        ```shell 
        gmsh CI02/CI02_gmsh.msh
        ```
        
   2. `CI02_rheolef.geo` that correspond to the `CI02_gmsh.msh` but compatible with [`rheolef`](https://membres-ljk.imag.fr/Pierre.Saramito/rheolef/html/index.html) library and with the code [`rheolef_cti`](https://gricad-gitlab.univ-grenoble-alpes.fr/mecaiceige/tools/ice-polycrystal-models/rheolef_cti)

From the python script you also have the input file compatible with `craft` :

     - `CI02_3d_micro.vtk`. It can be visualized using `paraview` 
        
        ```shell 
        paraview CI02_3d_micro.vtk
        ```
You also have `.phase` file containing euler angle of extracted from the AITA measurement. This file is compatible with `craft` but need to be adapted for `R3iCe`.

To do this use `ori_craft2rh.py` script

```shell
ori_craft2rh.py -i CI02_3d.phase -o CI02
```

You obtain `CI02.rhori` compatible with [`R3iCe`](https://gricad-gitlab.univ-grenoble-alpes.fr/mecaiceige/tools/ice-polycrystal-models/rheolef_cti).

#### Generate 2d mesh

##### Export file using [`xarrayaita`](https://gricad-gitlab.univ-grenoble-alpes.fr/mecaiceige/tools/lib_python/xarray_aita)

Here is an example of python script to run in order to obtain `craft` input file and `neper` compatible file `.tesr`.

```python
import xarrayaita.loadData_aita as lda
import xarrayaita.aita as xa
import numpy as np

adr_dat='/data/Manips/Columnar_Ice/CI02/DataBrut/AITA/CI02_aita.dat'
adr_micro='/data/Manips/Columnar_Ice/CI02/DataBrut/AITA/CI02_micro.bmp'

ds=lda.aita5col(adr_dat,adr_micro)

pos=[[1.4,75],[8,89.6]]
ds=ds.aita.crop(pos)
ds=ds.aita.rot180()

ds.aita.craft('CI02_2d',res=0.2,tesr=True)
```

##### Generate mesh from `.tesr` file

Run

```shell
mesh_size = 0.3
output=CI02_2d
input_tesr=CI02_2d_micro.tesr

NT_AITA_2d.sh $input_tesr $output $mesh_size
```

##### Output

Two output are given in folder `CI02/`:

   1. `CI02_gmsh.msh`. It can be visualized using `gmsh`:
        
        ```shell 
        gmsh CI02/CI02_gmsh.msh
        ```
        
   2. `CI02_rheolef.geo` that correspond to the `CI02_gmsh.msh` but compatible with [`rheolef`](https://membres-ljk.imag.fr/Pierre.Saramito/rheolef/html/index.html) library and with the code [`R3iCe`](https://gricad-gitlab.univ-grenoble-alpes.fr/mecaiceige/tools/ice-polycrystal-models/rheolef_cti)

From the python script you also have the input file compatible with `craft` :

     - `CI02_2d_micro.vtk`. It can be visualized using `paraview` 
        
        ```shell 
        paraview CI02_2d_micro.vtk
        ```
You also have `.phase` file containing euler angle of extracted from the AITA measurement. This file is compatible with `craft` but need to be adapted for `R3iCe`.

To do this use `ori_craft2rh.py` script

```shell
ori_craft2rh.py -i CI02_3d.phase -o CI02
```

You obtain `CI02.rhori` compatible with [`R3iCe`](https://gricad-gitlab.univ-grenoble-alpes.fr/mecaiceige/tools/ice-polycrystal-models/rheolef_cti).

```{note}
The `*.rhori` file contains two bunge euler angles $\phi_1$ and $\phi$ but when using [`R3iCe`](https://gricad-gitlab.univ-grenoble-alpes.fr/mecaiceige/tools/ice-polycrystal-models/rheolef_cti) only $\phi_1$ will be taken into account using a 2d mesh. The 3d orientation is projected in the plane $xOy$ :

$$c=(cos(\phi_1),sin(\phi_1)) $$
```